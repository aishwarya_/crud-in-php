<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Contact Application</title>
</head>

<body>

    <header>
        <h1>Contact App</h1>
    </header>

    <form action="adddata.php" method="post" enctype="multipart/form-data">
        <div class="main">
            <label for="name">Name </label><br>
            <input type="text" name="name" id="name" required><br><br>

            <label for="contact">Phone No. </label><br>
            <input type="number" name="contact" id="contact" required><br><br>

            <label for="email">Email</label><br>
            <input type="email" name="email" id="email" required><br><br>

            <label for="image">Image</label><br><br>
            <input type="file" name="image" id="image" required> <br><br>
            <input type="submit" class="submit_button" value="Submit" name="upload">


        </div>
    </form>
    <hr>

    <h2>List of Contacts</h2>
    <table>
        <tr>
            <th>#</th>
            <th>Name </th>
            <th>Phone No.</th>
            <th>Email</th>

            <th>Actions</th>


        </tr>

        <?php

        include 'db.php';

        $sql = "SELECT * FROM names ORDER BY id DESC";

        $result = mysqli_query($conn, $sql);

        if ($result) {
            while ($row = mysqli_fetch_assoc($result)) {
                $id = $row['id'];
                $name = $row['name'];
                $phone = $row['phone'];
                $email = $row['email'];

                
        ?>
                <tr>
                    <td><?php echo ++$i ?></td>
                    <td><?php echo $name ?></td>
                    <td><?php echo $phone ?></td>
                    <td><?php echo $email ?></td>

                    <td>
                        <a href="edit.php?id=<?php echo $id ?>">Update</a>
                        <a href="delete.php?id=<?php echo $id ?>">Delete</a>

                    </td>

                </tr>

        <?php
            }
        }

        ?>


    </table>
</body>

</html>